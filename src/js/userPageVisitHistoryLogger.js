/**
 * 
 */
(function($, window, document, undefined) {

  function userPageVisitHistoryLogger(options) {
    
    /**
     * Current options set by the caller including defaults.
     * @public
     */
    this.options = $.extend({}, userPageVisitHistoryLogger.Defaults, options);
    
    var parsedLimit = parseInt(this.options.itemsLimit);
    this.options.itemsLimit = (parsedLimit != NaN && parsedLimit > 0) 
                              ? parsedLimit 
                              : userPageVisitHistoryLogger.Defaults.itemsLimit;
    this.storage = null;
    
    if (typeof window.localStorage != 'undefined') {
      if (typeof window.localStorage.pageVisitHistory == 'undefined' || !window.localStorage.pageVisitHistory) {
        window.localStorage.pageVisitHistory = JSON.stringify([]);
      }
      
      this.storage = window.localStorage.pageVisitHistory;
    }
    
    if (document.referrer 
        && document.referrer.indexOf(location.protocol + "//" + location.host) == -1
        && this.options.addExtarnalRefererUrl) {
      
      if (this.options.clearOnExternalReffere) {
        this.clear();
      }
      
      this.addPage(document.referrer);
    }
    
    this.addPage(window.location.href);
    
  };
  
  /**
   * Default options for the userVisitHistory.
   * @public
   */
  userPageVisitHistoryLogger.Defaults = {
    itemsLimit: 0,
    addExtarnalRefererUrl: true,
    clearOnExternalReffere: true,
  };
  
  userPageVisitHistoryLogger.prototype.clear = function() {
    if (this.storage == null) {
      console.warn('Local storage not initialized and can\'t becleared.');
      return false;
    }
    
    window.localStorage.pageVisitHistory = JSON.stringify([]);
    
    return true;
  }
  
  userPageVisitHistoryLogger.prototype.addPage = function(url) {
    if (this.storage == null) {
      console.warn('Local storage not initialized - can\'t add page url to it.');
      return false;
    }
    var urlsList = this.getUrlsList(),
        item = {
          'url': url,
          'date': new Date(),
        };
    
    if (!urlsList) return false;
    
    urlsList.push(item);
    
    window.localStorage.pageVisitHistory = JSON.stringify(urlsList);
    
    return item;
  };
  
  userPageVisitHistoryLogger.prototype.getUrlsList = function() {
    
    var list = false;
    
    try {
      list = JSON.parse(window.localStorage.pageVisitHistory);
    } catch (e) {
      console.error('Error while parsing data from storage by getUrlsList function.');
    } 
    return list;
  }
  
  window.userPageVisitHistoryLogger = userPageVisitHistoryLogger;
  
})(window.jQuery, window, document);